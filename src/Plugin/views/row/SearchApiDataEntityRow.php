<?php

namespace Drupal\entity_view_mode_normalize\Plugin\views\row;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\rest\Plugin\views\row\DataEntityRow;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Plugin which displays entities as raw data.
 *
 * @ingroup views_row_plugins
 *
 * @ViewsRow(
 *   id = "search_api_data_entity_row",
 *   title = @Translation("Search API: source entity for search index"),
 *   help = @Translation("Search API: source entity for search index"),
 *   display_types = {"data"}
 * )
 */
class SearchApiDataEntityRow extends DataEntityRow {

  /**
   * Symfony\Component\HttpFoundation\RequestStack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Symfony\Component\HttpFoundation\Request|null.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected $request;

  /**
   * The serializer which serializes the views result.
   *
   * @var \Symfony\Component\Serializer\Serializer
   */
  protected $serializer;

  /**
   * {@inheritdoc}
   */
  protected $usesOptions = TRUE;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    LanguageManagerInterface $language_manager,
    EntityRepositoryInterface $entity_repository,
    RequestStack $requestStack,
    EntityDisplayRepositoryInterface $entity_display_repository = NULL,
    SerializerInterface $serializer
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $entity_type_manager,
      $language_manager,
      $entity_repository
    );
    $this->requestStack = $requestStack;
    $this->request = $requestStack->getCurrentRequest();
    $this->entityDisplayRepository = $entity_display_repository;
    $this->serializer = $serializer;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
      $container->get('entity.repository'),
      $container->get('request_stack'),
      $container->get('entity_display.repository'),
      $container->get('serializer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function render($row) {
    /** @var  \Drupal\search_api\Item\Item $item */
    $item = $row->_item;
    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $item->getOriginalObject()
      ->getValue();
    /*if ($entity instanceof ParagraphInterface) {
      return $entity;
    }*/
    $context = [];
    $context['field']['settings']['view_mode'] = $this->options['view_mode'];
    $context['view_row_plugin'] = $this->getPluginId();

    if ((empty($this->view->live_preview))) {
      $content_type = $this->displayHandler->getContentType();
    }
    else {
      $formats = [];
      if (!empty($this->view->getDisplay()->display['display_options']['style']['options']['formats'])) {
        $formats = $this->view->getDisplay()->display['display_options']['style']['options']['formats'];
      }
      $content_type = !empty($formats) ? reset($formats) : 'json';
    }

    return $this->serializer->normalize($entity, $content_type, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeId() {
    return empty($this->options['entity_type']) ? 'node' : $this->options['entity_type'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $entity_type = $this->entityTypeManager->getDefinitions();
    $options = [];
    foreach ($entity_type as $key => $item) {
      $options[$key] = $item->getLabel();
    }
    $form['entity_type'] = [
      '#type' => 'select',
      '#options' => $options,
      '#title' => $this->t('Entity type'),
      '#default_value' => $this->options['entity_type'],
    ];
    if (!empty($this->options['entity_type'])) {
      $form['view_mode'] = [
        '#type' => 'select',
        '#options' => $this->entityDisplayRepository->getViewModeOptions($this->options['entity_type']),
        '#title' => $this->t('View mode'),
        '#default_value' => $this->options['view_mode'],
      ];
    }

  }

}
