<?php

namespace Drupal\entity_view_mode_normalize\Normalizer;

use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\Core\TypedData\TypedDataInternalPropertiesHelper;
use Drupal\file\FileInterface;
use Drupal\serialization\Normalizer\EntityNormalizer;

/**
 * Converts typed data objects to arrays.
 */
class FileEntityNormalizer extends EntityNormalizer {

  /**
   * {@inheritdoc}
   */
  public function supportsNormalization($data, string $format = NULL, array $context = []): bool {
    if (!is_object($data) || !$this->checkFormat($format)) {
      return FALSE;
    }
    if ($data instanceof FileInterface) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = []): array|string|int|float|bool|\ArrayObject|NULL {
    $attributes = [];
    if ($object instanceof ComplexDataInterface) {
      // If there are no properties to normalize, just normalize the value.
      $object = !empty($object->getProperties(TRUE))
        ? TypedDataInternalPropertiesHelper::getNonInternalProperties($object)
        : $object->getValue();
    }
    $config_fields = \Drupal::service('entity_field.manager')
      ->getFieldDefinitions('file', 'default');
    /** @var \Drupal\Core\TypedData\TypedDataInterface $property */
    foreach ($object as $name => $property) {
      if ($name == 'uid') {
        continue;
      }

      if (!empty($config_fields[$name])) {
        $context['cardinality'] = $config_fields[$name]->getFieldStorageDefinition()
          ->getCardinality();
      }
      $attributes[$name] = $this->serializer->normalize($property, $format, $context);
    }

    $url = \Drupal::service('file_url_generator')->generateAbsoluteString($object->getFileUri());
    $attributes['url'] = \Drupal::service('file_url_generator')->transformRelative($url);
    $attributes['absolute_url'] = $url;
    return $attributes;
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedTypes(?string $format): array {
    return [
      'Drupal\file\FileInterface' => TRUE,
    ];
  }

}
