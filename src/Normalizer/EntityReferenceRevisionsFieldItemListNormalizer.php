<?php

namespace Drupal\entity_view_mode_normalize\Normalizer;

use Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList;
use Drupal\serialization\Normalizer\FieldItemNormalizer;


/**
 * Class Paragraph Normalizer.
 *
 * @package Drupal\entity_view_mode_normalize\EntityReferenceRevisionsFieldItemListNormalizer
 */
class EntityReferenceRevisionsFieldItemListNormalizer extends FieldItemNormalizer {

  use CardinalityItemTrait;

  /**
   * {@inheritdoc}
   */
  public function supportsNormalization($data, string $format = NULL, array $context = []): bool {
    if (!is_object($data) || !$this->checkFormat($format)) {
      return FALSE;
    }
    if ($data instanceof EntityReferenceRevisionsFieldItemList) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = []): array|string|int|float|bool|\ArrayObject|NULL {
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();

    $entities = $object->referencedEntities();
    foreach ($entities as &$entity) {
      if ($entity->hasTranslation($language)) {
        $entity = $entity->getTranslation($language);
      }
    }
    $attributes = $this->serializer->normalize($entities, $format, $context);
    return $this->getItemByCardinalityContext($attributes, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedTypes(?string $format): array {
    return [
      EntityReferenceRevisionsFieldItemList::class => TRUE,
    ];
  }
}
